﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BruteAttack : MonoBehaviour
{
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public int attackDamage;
    public float cooldown = 3;
    public bool cooldownActive = false;

    // Start is called before the first frame update
    void Start()
    {
        cooldownActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldownActive)
            if (cooldown > 0)
            {
                cooldown -= Time.deltaTime;
            }

            else
            {
                Attack();
                cooldown = 3;
            }
        if (Input.GetKeyDown(KeyCode.C))
        {
            Destroy(gameObject);
        }
           
    }

    void Attack()
    {
        //enemies in range for attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<playerHealth>().TakeDamage(attackDamage);
        }

    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
