﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BruteHealth : MonoBehaviour
{
    public int maxHealth = 100;
    int currentHealth;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        //place any ink drops here or other events on death

        Destroy(gameObject);

    }
}
