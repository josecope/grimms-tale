﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersoundTracker : MonoBehaviour
{
    public AudioSource[] soundFX; //create an array of audiosources

    private bool isOnGround;// allows for a true or false condition as to whether player is on the ground

    private Rigidbody2D rb; // allows us to access the player's rigidbody2d

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // gets and stores reference to rigidbody for use
        isOnGround = true;
    }

    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "ground" || other.gameObject.tag == "enemy" || other.gameObject.tag == "spider") //checks if player has touched the ground or any of the other game objects
        {
            isOnGround = true; //if the player has hit the ground, then changes it to true so you can jump again
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround) // activates the audio command when W is pressed down and the player is on the ground
        {
            {
                isOnGround = false; // tells the system that the player is not on the ground
                soundFX[0].Play(); //plays the audio file in array location 0
            }
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D)) // activates the audio command when A or D is pressed down
        {
            { 
                soundFX[1].Play(); //plays the audio file in array location 1
            }
        }

    }
}
