﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public float moveSpeed = 7f;

    public Rigidbody2D rb;

    public GameObject target;
    public Vector2 moveDirection;

    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask playerLayer;
    public int attackDamage;




    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindWithTag("Player");
        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
        Destroy(gameObject, 10f);

    }

    void Update()
    {
        Attack();
        

    }

    void Attack()
    {
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);

        foreach (Collider2D player in hitPlayer)
        {
            UnityEngine.Debug.Log("hit player!");
            player.GetComponent<playerHealth>().TakeDamage(attackDamage);
            Destroy(gameObject);
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "ground" || other.gameObject.tag == "monster")
        {
            Destroy(gameObject);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

}
