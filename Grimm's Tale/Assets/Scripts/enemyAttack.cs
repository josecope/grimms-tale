﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class enemyAttack : MonoBehaviour
{
    public GameObject bullet;

    public float fireRate;
    public float nextFire;

    private void Start()
    {

        fireRate = 5f;
        nextFire = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        Shoot();

    }

    void Shoot()
    {

        if(Time.time > nextFire)
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
            nextFire = Time.time + fireRate;
        }

    }
}
