﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inkBox : MonoBehaviour
{

    public Sprite newSprite;
    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<Collider2D>().enabled == true)
        {
            spriteRenderer.sprite = newSprite;
        }
    }
}
