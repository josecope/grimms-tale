﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class madlibFinish : MonoBehaviour
{
    //set a public integer to count the number of words that have been put into the madlib
    public int wordCount;

    public bool moving;

    // Start is called before the first frame update
    void Start()
    {
        wordCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            moving = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            moving = false;
        }

        if (wordCount == 2)
        {

            newScene();

        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "word")
        {
            wordCount = wordCount + 1;

        }
    }

    void newScene()
    {
        if(moving == false)
        {
            SceneManager.LoadScene("Level03");
        }
    }

}
