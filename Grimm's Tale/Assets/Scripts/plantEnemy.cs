﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plantEnemy : MonoBehaviour
{
    public int maxHealth = 100;
    int currentHealth;
    public Sprite newSprite;
    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage) 
    {
        currentHealth -= damage;

        if(currentHealth <= 0) 
        {
            Die();
        }
    }

    public void Die() 
    {
        if (this.gameObject.tag == "spider")
        {
            GetComponent<spiderMovement>().enabled = false;
            sceneManager.letterCount += 1;
            gameObject.layer = 10;
            spriteRenderer.sprite = newSprite;
            //Destroy(gameObject);

        }
        else if(this.gameObject.tag == "monster")
        {
            GameObject.Find("Ink Box").GetComponent<Collider2D>().enabled = true;
            Destroy(gameObject);
        }
        else
        {
            
            Destroy(gameObject);
        }

    }

}
