﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;

public class playerAttack : MonoBehaviour
{
    public GameObject letter;
    public Animator animator;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public LayerMask letterLayers;
    public LayerMask itemLayers;
    public int attackDamage;

    // Start is called before the first frame update
    void Start()
    {
       // letter = GameObject.Find("Letter");
       // letter.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }


        if (Input.GetKeyDown(KeyCode.E))
        {
            Pickup();
        }

    }

    void Attack() 
    {
        //play attack animation
        animator.SetTrigger("Attack");

        //enemies in range for attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (Collider2D enemy in hitEnemies) 
        {
            enemy.GetComponent<plantEnemy>().TakeDamage(attackDamage);
        }

    }

    void Pickup()
    {
        animator.SetTrigger("Attack");

        Collider2D[] hitLetters = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, letterLayers);

        foreach (Collider2D letter in hitLetters)
        {
            GameObject.Find("Gate Wall").GetComponent<Collider2D>().enabled = false;
            Destroy(letter.gameObject);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null) 
        {
            return;
        }

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

}