﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerMovement : MonoBehaviour
{

    public float speed; // sets player speed as a floating public variable to make changing easy

    public CapsuleCollider2D collide2D;

    private Rigidbody2D rb; // allows us to access the player's rigidbody2d

    public float jumpForce; // allows us to set the force of the player's jump

    public Animator animator;

    public float inkCharges; // keeps count of the player's ink charges

    public float mySpeed; // Adjustable speed for the player

    public bool isOnGround; // allows for a true or false condition as to whether player is on the ground

    public float inkJumpCoolDown; // keeps track of the player's cooldown time

    public float nextInkJump; // sets time until next ink jump can be done

    private SpriteRenderer mySpriteRenderer;

    public GameObject speech;


    private void Awake()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>(); //gets spriteRenderer

        collide2D = GetComponent<CapsuleCollider2D>();
    }

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();  // gets and stores reference to rigidbody for use

        isOnGround = true;

        speech = GameObject.Find("Speech");

        speech.SetActive(false);
       

    }

    // Update is called once per frame
    void Update()
    {

        rb.velocity = new Vector2(mySpeed, rb.velocity.y);

        

        if (Input.GetKeyDown(KeyCode.Space) && isOnGround) // activates the jump command when W is pressed down and the player is on the ground
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            isOnGround = false;
            animator.SetTrigger("Jump");
        }

        if (Input.GetKey(KeyCode.DownArrow)) //while this key is pressed you crouched
        {
            collide2D.size = new Vector2(8, 2.25f);
            collide2D.offset = new Vector2(collide2D.offset.x, -2.5f);
            collide2D.direction = CapsuleDirection2D.Horizontal;
            animator.SetBool("Slide", true);



        }
        else if (Input.GetKeyUp(KeyCode.DownArrow)) //when key is up you stand normal
        {
            collide2D.size = new Vector2(3.68f, 9.42f);
            collide2D.offset = new Vector2(0.1f, 0f);
            collide2D.direction = CapsuleDirection2D.Vertical;            
            animator.SetBool("Slide", false);
        }

    }

    void inkJump() // MAPPED TO 1
    {

        if (Input.GetKeyDown(KeyCode.Alpha1) && inkCharges >= 1 && Time.time > nextInkJump) // activates the method when 1 is pressed down and the player has at least 1 ink charge
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce * 2 - 8); // adjusts the new velocity to account for the force of jumping, which is then multiplied by 2 and brought down by 8 to make for a greater jump
            isOnGround = false; // tells the system that the player is not on the ground
            inkCharges = inkCharges - 1; // lowers the number of ink charges that the player has by 1
            nextInkJump = Time.time + inkJumpCoolDown; // sets a cool down timer for the ability

            animator.SetTrigger("SuperJump");
           

        }

    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.gameObject.tag == "ground") //check to see if the player touches the ground
        {
            isOnGround = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "speechOn")
        {
            speech.SetActive(true);
        }

        if(other.gameObject.tag == "speechOff")
        {
            speech.SetActive(false);
        }
    }


}