﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour
{
    public string newScene;

    public static int letterCount = 0;

    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(letterCount == 1)
        {
            SceneManager.LoadScene(newScene, LoadSceneMode.Single);
        }
    }
}
