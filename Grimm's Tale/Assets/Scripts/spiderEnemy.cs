﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiderEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite newSprite;
    public SpriteRenderer spriteRenderer;

    public int maxHealth;
    int spiderCurrentHealth;

    // Start is called before the first frame update
    void Start()
    {
        enemyBase refScript = GetComponent<enemyBase>();
        
        enemyBase.maxHealth = maxHealth;
        spiderCurrentHealth = maxHealth;
        
    }

    public void Update()
    {

        
       
    }

    
    public void TakeDamage(int damage)
    {

        GetComponent<enemyBase>().TakeDamage(damage);
        spiderCurrentHealth = GetComponent<enemyBase>().currentHealth;
        
        if(spiderCurrentHealth <= 0)
        {
            Die();
        }
       
    }

    public void Die()
    {
        gameObject.layer = 10;
        spriteRenderer.sprite = newSprite;
        sceneManager.letterCount += 1;
        GetComponent<spiderMovement>().enabled = false;

    }
}
