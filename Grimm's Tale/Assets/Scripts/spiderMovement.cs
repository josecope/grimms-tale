﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiderMovement : MonoBehaviour
{
    private bool dirRight = true;
    public float speed = 2.0f;
    public AudioSource[] soundFX;

    void Update()
    {
        soundFX[0].Play();

        if (dirRight)
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        else
            transform.Translate(-Vector2.right * speed * Time.deltaTime);

        if (transform.position.x >= 52.0f)
        {
            dirRight = false;
        }

        if (transform.position.x <= 39)
        {
            dirRight = true;
        }
    }
}

